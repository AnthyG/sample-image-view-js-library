# Sample Image View JS-Library

Has to be used in combination with a [Sample Image Data Zite](https://gitlab.com/AnthyG/sample-image-data-zite).

Clone this repo into a new zite, edit the `ignore` property in it's `content.json` to look like follows:

`"ignore": "((js|css)/(?!all.(js|css))|.git)"`

## To set the library up on a separate zite, follow the steps below

### Set-up

Copy the library from `js/imgviewlib.js` to your zite.

Edit the basic ZeroPage JS (which is normally in a `<script></script>` in the default `index.html` of any new zite) as follows:

Add `page.site_info = site_info;` to the function `setSiteInfo`:

```javascript
...
setSiteInfo(site_info) {
    page.site_info = site_info; // make sure that the site_info is available for ImgViewLib
...
```

Add `window.imgview = window.ImgViewLib(page);` after the first cmd call of `siteInfo` (normally in `onOpenWebsocket`):

```javascript
...
onOpenWebsocket() {
    this.cmd("siteInfo", [], function(site_info) {
        window.imgview = window.ImgViewLib(page); // Initialize ImgViewLib
...
```

### Configuration

ImgViewLib allows for two separate ways of configuration. The basic one has a "one-time set-up" of default options, whilst the advanced one provides the ability to set options per element but uses the basic set-up as a fallback.

#### Basic

To configure default options, add the following before loading the library (for example in the `<head></head>`):

```html
<script>
    window.imgviewlib_opt = {
        datazite: "1D2xzWx2oG8oUiMpKvygeoyYL7uJXFMp8k",
        thumbnailspath: "data/thumbnails.json",
        datapath: "data/images"
    };
</script>
```

property | possible values
--- | ---
`datazite` | The Address of the [Sample Image Data Zite](https://gitlab.com/AnthyG/sample-image-data-zite)
`thumbnailspath` | A path relative to the given `datazite`, without any preceding `/`
`datapath` | A path relative to the given `datazite`, pointing to where e.g. images are stored, without any preceding or succeeding `/`

The only mandatory option to be set is `datazite` if the advanced way is not being used.

To then use ImgViewLib, you can add HTML-elements each with the attribute `imgviewlib-file` set to the filename. On initialization, ImgViewLib will search for all the elements with said attribute, and automatically do the "backend"-work. To use the image data that ImgViewLib fetches, you may add as many child nodes through the following two options as you wish, to the element which has the `imgviewlib-file` attribute:

- using `imgviewlib-img` as an attribute will set the `src` attribute of the assigned element:
    ```html
    <img imgviewlib-img />
    ```
- using `imgviewlib-bg` as an attribute will set the CSS `background-image` of the assigned element:
    ```html
    <div imgviewlib-bg style="height: 250px; background-size: contain; background-repeat: no-repeat;"></div>
    ```

E.g. a full element:

```html
<!-- use JS options object -->

<div imgviewlib-file="1556996996290.jpeg">
    <!-- assign image through src-attribute -->
    <img imgviewlib-img height="250" />

    <!-- assign image through CSS background-image -->
    <div imgviewlib-bg style="height: 250px; background-size: contain; background-repeat: no-repeat;"></div>
</div>
```

Furthermore, ImgViewLib provides the ability to bind specific eventhandlers to the `onclick` event as well as outputting internal variables to elements. The following table describes what each of the bindings does:

Attribute name | Bound-to event | Binding effect
--- | --- | ---
`imgviewlib-getinfo` | onclick | calls `checkDatafileInfo`
`imgviewlib-load` | onclick | calls `requestDatafileLoad`
`imgviewlib-delete` | onclick | calls `requestDatafileDelete`
`imgviewlib-filename` |  | `innerText` gets replaced by the `filename`

To assign any of the bindings:

```html
<div imgviewlib-file="1556996996290.jpeg">
    <button imgviewlib-getinfo>Get info</button>
    <button imgviewlib-load>Load file</button>
    <button imgviewlib-delete>Delete file</button>
    <span imgviewlib-filename></span>

    <img imgviewlib-img />
</div>
```

#### Advanced

ImgViewLib also lets you specify options for each file separately by instead of using the `imgviewlib-file` attribute, using the `imgviewlib` attribute containing an options object.
The `filename` property of said object is the only mandatory property, if also using the default options set-up as described in the basic configuration section, if not, then the `datazite` property is mandatory too. Any of the options mentioned in the basic configuration section may be used.
This allows to fetch information from multiple data-zites or from multiple places of the same data-zite (e.g. using a different thumbnails file for other image categories).

See the following, on how to use the specific options object:

```html
<!-- use JS options object as base and override options through attribute -->

<div imgviewlib='{ "filename": "1556996996290.jpeg" }'>
    <img imgviewlib-img />
</div>

<div imgviewlib='{ "thumbnailspath": "data/thumbnails2.json", "filename": "1556996996290.jpeg" }'>
    <img imgviewlib-img />
</div>
```

## Internals

> This section describes how the functions of ImgViewLib work

### Variables

#### zitedata

The `zitedata` variable holds thumbnails fetched from data-zites and the respective elements that are bound to by ImgViewLib as well as the image-output elements.

Its structure is as follows (everything in `'` is evaluated as code/ variable):

```javascript
'zitedata' = {
    'datazite': {
        "thumbnails": {
            'filename': 'thumbnail-data'
        },
        "elements": {
            'datapath + "/" + filename': [
                {
                    "el": 'el', // the parent element that has the `imgviewlib-file` or `imgviewlib` attribute
                    "img": [], // contains references to all image-output elements that use the `imgviewlib-img` attribute
                    "bg": [] // contains references to all image-output elements that use the `imgviewlib-bg` attribute
                },
                ...
            ]
        }
    }
}
```

#### opt

This variable holds the default options provided by ImgViewLib and is used as a fallback if no default options are given as described in the basic configuration section.

#### ELs_obj and ELs_file

`ELs_obj` and `ELs_file` both contain the references to the respective elements that either use `imgviewlib` or `imgviewlib-file` as attributes.

### Functions

#### getThumbnails

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`thumbnailspath` |  | `opt.thumbnailspath`
`cb` | `function` |

Functionality:

Calls `corsCmd` first, and on callback it uses `fileGet` to fetch the thumbnails file as described per `datazite` and `thumbnailspath`.
`cb` receives the newly fetched thumbnails object.

#### requestDatafileLoad

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`datapath` | a path as described in the basic configuration section | `opt.datapath`
`filename` | a string pointing to a file in `datapath` of `datazite` | the name of the optional file
`cb` | `function` |

Functionality:

Calls `corsCmd` first, and on callback it uses `fileGet` to attempt to fetch the image file as described per `datazite`, `datapath` and `filename` with the `required` argument set to `true` to make the ZeroNet client request the optional file.
`cb` receives the fetched file or anything returned by `fileGet`.

#### requestDatafileDelete

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`filename` | a string pointing to a file in `datapath` of `datazite` | the name of the optional file

Functionality:

Calls `corsCmd` first, and on callback redirects to the deleter page as described per `datazite` and `filename`.

#### checkDatafileInfo

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`datapath` | a path as described in the basic configuration section | `opt.datapath`
`filename` | a string pointing to a file in `datapath` of `datazite` | the name of the optional file
`cb` | `function` |

Functionality:

Calls `corsCmd` first, and on callback uses the `as` cmd to execute `optionalFileInfo` as described per `datazite` and `filepath` (which is `datapath + "/" + filename`).
Then updates all the ImgViewLib-bound image-output elements related to the `filepath` (those are referenced in `zitedata`).

#### corsCmd

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`cb` | `function` |

Functionality:

This function checks for the CORS permission as described per `datazite` and calls `cb` if the permission is granted or has already been granted.

#### bindingUpdater

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`_opt` | a option object as described in the basic configuration section | `opt`
`filename` | a string pointing to a file in `datapath` of `datazite` | the name of the optional file
`el` | a HTML node |

Functionality:

Updates all the ImgViewLib-bound elements as described per `el`. Sets all the image-outputs to the respective thumbnails.
Returns an object that contains all the node lists.

#### elementBinder

Parameters:

name | possible value/ type | default value/ type
--- | --- | ---
`datazite` | address to a data-zite | `opt.datazite`
`_opt` | a option object as described in the basic configuration section | `opt`
`filename` | a string pointing to a file in `datapath` of `datazite` | the name of the optional file
`el` | a HTML node |

Functionality:

Calls `bindingUpdater` as described per `datazite`, `_opt`, `filename` and `el` after which it populates `zitedata` with the relevant elements that are returned by `bindingUpdater` and `el` as described per `datazite` and `_opt.datapath + "/" + filename`.

Then calls `checkDatafileInfo` as described per `datazite`, `_opt.datapath` and `filename` and finally adds a class `imgviewlib-loaded` to `el`.

### Procedure

First, `opt` gets assigned the given default options as described in the basic configuration section (`window.imgviewlib_opt`), then all the elements that either have the `imgviewlib` or `imgviewlib-file` attribute are saved to the respective variables as described in variables section of the internals section.

Then `opt.datazite` gets checked and if `true`, `getThumbnails` gets called as described per `opt.datazite` and `opt.thumbnailspath`. In the `cb` of that `getThumbnails` is a function that loops through all elements in `ELs_file` and calls `elementBinder` as described per `opt.datazite`, `opt`, `filename` and the respective element.

It also executes a similar procedure for the elements that have the `imgviewlib` attribute, but also extracting the specific options object from each element, merging it with the default options and then calling `getThumbnails` as described per the specific `datazite` and the specific `thumbnailspath` (both in `opt2`, which is the specific merged options object). The `cb` of `getThumbnails` then is a function that calls `elementBinder` as described per `opt2` and the respective element.