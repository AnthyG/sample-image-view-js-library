ImgViewLib = function(page) {
    zitedata = {};

    opt = {
        datazite: "",
        thumbnailspath: "data/thumbnails.json",
        datapath: "data/images"
    };

    if (window.imgviewlib_opt && typeof window.imgviewlib_opt === "object") {
        Object.assign(opt, window.imgviewlib_opt);
    }

    let ELs_obj = document.querySelectorAll("[imgviewlib]");
    let ELs_file = document.querySelectorAll("[imgviewlib-file]");

    console.log("ImgViewLib >>", opt, ELs_obj, ELs_file);

    getThumbnails = function(datazite = opt.datazite, thumbnailspath = opt.thumbnailspath, cb) {
        if (datazite && thumbnailspath) {
            corsCmd(datazite, function() {
                page.cmd("fileGet", {
                    "inner_path": "cors-" + datazite + "/" + thumbnailspath,
                    "required": true
                }, (_thumbnails) => {
                    if (_thumbnails) {
                        _thumbnails = JSON.parse(_thumbnails);

                        if (!zitedata.hasOwnProperty(datazite)) {
                            zitedata[datazite] = {};
                        }
                        zitedata[datazite].thumbnails = _thumbnails;

                        typeof cb === "function" && cb(_thumbnails);
                    }
                });
            });
        }
    };

    requestDatafileLoad = function(datazite = opt.datazite, datapath = opt.datapath, filename, cb) {
        if (datazite && filename) {
            let filepath = "cors-" + datazite + "/" + datapath + "/" + filename;

            console.log("datafile load requesting..", filepath);

            corsCmd(datazite, function() {
                page.cmd("fileGet", [filepath, true, "base64", 1000], (res) => {
                    console.log("datafile load request", filepath);

                    typeof cb === "function" && cb(res);
                });
            });
        }
    };

    requestDatafileDelete = function(datazite = opt.datazite, filename) {
        corsCmd(datazite, function() {
            page.cmd("wrapperOpenWindow", ["/" + datazite + "/deleter.html?viewzite=" + page.site_info.address + "&filename=" + filename, "_self"]);
        });
    };

    checkDatafileInfo = function(datazite = opt.datazite, datapath = opt.datapath, filename, cb) {
        let filepath = datapath + "/" + filename;

        corsCmd(datazite, function() {
            page.cmd("as", [datazite, "optionalFileInfo", [filepath]], (res) => {
                if (!res) {
                    res = {
                        "is_downloaded": false
                    };
                }

                console.log("got optional file info", datazite, filename, res, zitedata[datazite].elements[filepath]);

                for (let elcI = 0; elcI < zitedata[datazite].elements[filepath].length; elcI++) {
                    let elc = zitedata[datazite].elements[filepath][elcI];

                    let ELs_img = elc.img;
                    let ELs_bg = elc.bg;

                    for (let el_imgI = 0; el_imgI < ELs_img.length; el_imgI++) {
                        let el_img = ELs_img[el_imgI];
                        // console.log("setting thumbnail on el_img", el_img, zitedata[datazite].thumbnails[filename]);
                        el_img.src = res.is_downloaded ? ("cors-" + datazite + "/" + filepath) : zitedata[datazite].thumbnails[filename];
                    }
                    for (let el_bgI = 0; el_bgI < ELs_bg.length; el_bgI++) {
                        let el_bg = ELs_bg[el_bgI];
                        // console.log("setting thumbnail on el_bg", el_bg, zitedata[datazite].thumbnails[filename]);
                        el_bg.style.backgroundImage = "url('" + (res.is_downloaded ? ("cors-" + datazite + "/" + filepath) : zitedata[datazite].thumbnails[filename]) + "')";
                    }
                }

                typeof cb === "function" && cb(res, filename);
            });
        });
    };

    corsCmd = function(datazite = opt.datazite, cb) {
        let corsaddress = "Cors:" + datazite;
        if (page.site_info.settings.permissions.indexOf(corsaddress) < 0) {
            // no permission yet granted
            page.cmd("corsPermission", [datazite], () => {
                // permission granted
                typeof cb === "function" && cb();
            });
        } else {
            // already got permission
            typeof cb === "function" && cb();
        }
    };

    bindingUpdater = function(datazite = opt.datazite, _opt = opt, filename, el) {
        let ELs_img = el.querySelectorAll("[imgviewlib-img]");
        let ELs_bg = el.querySelectorAll("[imgviewlib-bg]");

        let ELs_filename = el.querySelectorAll("[imgviewlib-filename]");

        let ELs_getinfo = el.querySelectorAll("[imgviewlib-getinfo]");
        let ELs_load = el.querySelectorAll("[imgviewlib-load]");
        let ELs_delete = el.querySelectorAll("[imgviewlib-delete]");

        // console.log("elements to bind to", el, ELs_img, ELs_bg, ELs_filename, ELs_getinfo, ELs_load, ELs_delete);

        for (let el2I = 0; el2I < ELs_img.length; el2I++) {
            let el2 = ELs_img[el2I];
            el2.src = zitedata[datazite].thumbnails[filename];
        }

        for (let el2I = 0; el2I < ELs_bg.length; el2I++) {
            let el2 = ELs_bg[el2I];
            el2.style.backgroundImage = "url('" + zitedata[datazite].thumbnails[filename] + "')";
        }

        for (let el2I = 0; el2I < ELs_filename.length; el2I++) {
            let el2 = ELs_filename[el2I];
            el2.innerText = filename;
        }

        for (let el2I = 0; el2I < ELs_getinfo.length; el2I++) {
            let el2 = ELs_getinfo[el2I];
            el2.onclick = function() {
                checkDatafileInfo(datazite, _opt.datapath, filename);
            };
        }

        for (let el2I = 0; el2I < ELs_load.length; el2I++) {
            let el2 = ELs_load[el2I];
            el2.onclick = function() {
                requestDatafileLoad(datazite, _opt.datapath, filename, function() {
                    checkDatafileInfo(datazite, _opt.datapath, filename);
                });
            };
        }

        for (let el2I = 0; el2I < ELs_delete.length; el2I++) {
            let el2 = ELs_delete[el2I];
            el2.onclick = function() {
                requestDatafileDelete(datazite, filename);
            };
        }

        return {
            "img": ELs_img,
            "bg": ELs_bg,
            "filename": ELs_filename,
            "getinfo": ELs_getinfo,
            "load": ELs_load,
            "delete": ELs_delete
        };
    };

    elementBinder = function(datazite = opt.datazite, _opt = opt, filename, el) {
        let ELs = bindingUpdater(datazite, _opt, filename, el);

        if (!zitedata[datazite].hasOwnProperty("elements")) {
            zitedata[datazite].elements = {};
        }
        if (!zitedata[datazite].elements.hasOwnProperty(_opt.datapath + "/" + filename)) {
            zitedata[datazite].elements[_opt.datapath + "/" + filename] = [];
        }
        if (zitedata[datazite].elements[_opt.datapath + "/" + filename].indexOf(el) < 0) {
            zitedata[datazite].elements[_opt.datapath + "/" + filename].push({
                "el": el,
                "img": ELs.img,
                "bg": ELs.bg
            });
        }

        checkDatafileInfo(datazite, _opt.datapath, filename);

        el.classList.add("imgviewlib-loaded");
    };

    if (opt.datazite) {
        let datazite = opt.datazite;
        getThumbnails(datazite, opt.thumbnailspath, function() {
            for (let elI = 0; elI < ELs_file.length; elI++) {
                let el = ELs_file[elI];

                let filename = el.getAttribute("imgviewlib-file");

                elementBinder(datazite, opt, filename, el);
            }
        });
    }

    for (let elI = 0; elI < ELs_obj.length; elI++) {
        let el = ELs_obj[elI];

        let opt2 = JSON.parse(JSON.stringify(opt));
        let opt3 = JSON.parse(el.getAttribute("imgviewlib"));
        Object.assign(opt2, opt3);

        let filename = opt2.filename;
        let datazite = opt2.datazite;

        getThumbnails(datazite, opt2.thumbnailspath, function() {
            elementBinder(datazite, opt2, filename, el);
        });
    }
};
window.ImgViewLib = ImgViewLib;