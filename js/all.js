
/* ---- /1PmkreYrHALCPaPQx3h9Ht8oQzjCKpA3n/js/ZeroFrame.js ---- */


// Version 1.0.0 - Initial release
// Version 1.1.0 (2017-08-02) - Added cmdp function that returns promise instead of using callback
// Version 1.2.0 (2017-08-02) - Added Ajax monkey patch to emulate XMLHttpRequest over ZeroFrame API
// Version 1.3.0 (2018-12-05) - Added monkey patch for fetch API

const CMD_INNER_READY = 'innerReady'
const CMD_RESPONSE = 'response'
const CMD_WRAPPER_READY = 'wrapperReady'
const CMD_PING = 'ping'
const CMD_PONG = 'pong'
const CMD_WRAPPER_OPENED_WEBSOCKET = 'wrapperOpenedWebsocket'
const CMD_WRAPPER_CLOSE_WEBSOCKET = 'wrapperClosedWebsocket'

class ZeroFrame {
    constructor(url) {
        this.url = url
        this.waiting_cb = {}
        this.wrapper_nonce = document.location.href.replace(/.*wrapper_nonce=([A-Za-z0-9]+).*/, "$1")
        this.connect()
        this.next_message_id = 1
        this.init()
    }

    init() {
        return this
    }

    connect() {
        this.target = window.parent
        window.addEventListener('message', e => this.onMessage(e), false)
        this.cmd(CMD_INNER_READY)
    }

    onMessage(e) {
        let message = e.data
        let cmd = message.cmd
        if (cmd === CMD_RESPONSE) {
            if (this.waiting_cb[message.to] !== undefined) {
                this.waiting_cb[message.to](message.result)
            }
            else {
                this.log("Websocket callback not found:", message)
            }
        } else if (cmd === CMD_WRAPPER_READY) {
            this.cmd(CMD_INNER_READY)
        } else if (cmd === CMD_PING) {
            this.response(message.id, CMD_PONG)
        } else if (cmd === CMD_WRAPPER_OPENED_WEBSOCKET) {
            this.onOpenWebsocket()
        } else if (cmd === CMD_WRAPPER_CLOSE_WEBSOCKET) {
            this.onCloseWebsocket()
        } else {
            this.onRequest(cmd, message)
        }
    }

    onRequest(cmd, message) {
        this.log("Unknown request", message)
    }

    response(to, result) {
        this.send({
            cmd: CMD_RESPONSE,
            to: to,
            result: result
        })
    }

    cmd(cmd, params={}, cb=null) {
        this.send({
            cmd: cmd,
            params: params
        }, cb)
    }

    cmdp(cmd, params={}) {
        return new Promise((resolve, reject) => {
            this.cmd(cmd, params, (res) => {
                if (res && res.error) {
                    reject(res.error)
                } else {
                    resolve(res)
                }
            })
        })
    }

    send(message, cb=null) {
        message.wrapper_nonce = this.wrapper_nonce
        message.id = this.next_message_id
        this.next_message_id++
        this.target.postMessage(message, '*')
        if (cb) {
            this.waiting_cb[message.id] = cb
        }
    }

    log(...args) {
        console.log.apply(console, ['[ZeroFrame]'].concat(args))
    }

    onOpenWebsocket() {
        this.log('Websocket open')
    }

    onCloseWebsocket() {
        this.log('Websocket close')
    }

    monkeyPatchAjax() {
        var page = this
        XMLHttpRequest.prototype.realOpen = XMLHttpRequest.prototype.open
        this.cmd("wrapperGetAjaxKey", [], (res) => { this.ajax_key = res })
        var newOpen = function (method, url, async) {
            url += "?ajax_key=" + page.ajax_key
            return this.realOpen(method, url, async)
        }
        XMLHttpRequest.prototype.open = newOpen

        window.realFetch = window.fetch
        var newFetch = function (url) {
            url += "?ajax_key=" + page.ajax_key
            return window.realFetch(url)
        }
        window.fetch = newFetch
    }
}

/* ---- /1PmkreYrHALCPaPQx3h9Ht8oQzjCKpA3n/js/imgviewlib.js ---- */


ImgViewLib = function(page) {
    zitedata = {};

    opt = {
        datazite: "",
        thumbnailspath: "data/thumbnails.json",
        datapath: "data/images"
    };

    if (window.imgviewlib_opt && typeof window.imgviewlib_opt === "object") {
        Object.assign(opt, window.imgviewlib_opt);
    }

    let ELs_obj = document.querySelectorAll("[imgviewlib]");
    let ELs_file = document.querySelectorAll("[imgviewlib-file]");

    console.log("ImgViewLib >>", opt, ELs_obj, ELs_file);

    getThumbnails = function(datazite = opt.datazite, thumbnailspath = opt.thumbnailspath, cb) {
        if (datazite && thumbnailspath) {
            corsCmd(datazite, function() {
                page.cmd("fileGet", {
                    "inner_path": "cors-" + datazite + "/" + thumbnailspath,
                    "required": true
                }, (_thumbnails) => {
                    if (_thumbnails) {
                        _thumbnails = JSON.parse(_thumbnails);

                        if (!zitedata.hasOwnProperty(datazite)) {
                            zitedata[datazite] = {};
                        }
                        zitedata[datazite].thumbnails = _thumbnails;

                        typeof cb === "function" && cb(_thumbnails);
                    }
                });
            });
        }
    };

    requestDatafileLoad = function(datazite = opt.datazite, datapath = opt.datapath, filename, cb) {
        if (datazite && filename) {
            let filepath = "cors-" + datazite + "/" + datapath + "/" + filename;

            console.log("datafile load requesting..", filepath);

            corsCmd(datazite, function() {
                page.cmd("fileGet", [filepath, true, "base64", 1000], (res) => {
                    console.log("datafile load request", filepath);

                    typeof cb === "function" && cb(res);
                });
            });
        }
    };

    requestDatafileDelete = function(datazite = opt.datazite, filename) {
        corsCmd(datazite, function() {
            page.cmd("wrapperOpenWindow", ["/" + datazite + "/deleter.html?viewzite=" + page.site_info.address + "&filename=" + filename, "_self"]);
        });
    };

    checkDatafileInfo = function(datazite = opt.datazite, datapath = opt.datapath, filename, cb) {
        let filepath = datapath + "/" + filename;

        corsCmd(datazite, function() {
            page.cmd("as", [datazite, "optionalFileInfo", [filepath]], (res) => {
                if (!res) {
                    res = {
                        "is_downloaded": false
                    };
                }

                console.log("got optional file info", datazite, filename, res, zitedata[datazite].elements[filepath]);

                for (let elcI = 0; elcI < zitedata[datazite].elements[filepath].length; elcI++) {
                    let elc = zitedata[datazite].elements[filepath][elcI];

                    let ELs_img = elc.img;
                    let ELs_bg = elc.bg;

                    for (let el_imgI = 0; el_imgI < ELs_img.length; el_imgI++) {
                        let el_img = ELs_img[el_imgI];
                        // console.log("setting thumbnail on el_img", el_img, zitedata[datazite].thumbnails[filename]);
                        el_img.src = res.is_downloaded ? ("cors-" + datazite + "/" + filepath) : zitedata[datazite].thumbnails[filename];
                    }
                    for (let el_bgI = 0; el_bgI < ELs_bg.length; el_bgI++) {
                        let el_bg = ELs_bg[el_bgI];
                        // console.log("setting thumbnail on el_bg", el_bg, zitedata[datazite].thumbnails[filename]);
                        el_bg.style.backgroundImage = "url('" + (res.is_downloaded ? ("cors-" + datazite + "/" + filepath) : zitedata[datazite].thumbnails[filename]) + "')";
                    }
                }

                typeof cb === "function" && cb(res, filename);
            });
        });
    };

    corsCmd = function(datazite = opt.datazite, cb) {
        let corsaddress = "Cors:" + datazite;
        if (page.site_info.settings.permissions.indexOf(corsaddress) < 0) {
            // no permission yet granted
            page.cmd("corsPermission", [datazite], () => {
                // permission granted
                typeof cb === "function" && cb();
            });
        } else {
            // already got permission
            typeof cb === "function" && cb();
        }
    };

    bindingUpdater = function(datazite = opt.datazite, _opt = opt, filename, el) {
        let ELs_img = el.querySelectorAll("[imgviewlib-img]");
        let ELs_bg = el.querySelectorAll("[imgviewlib-bg]");

        let ELs_filename = el.querySelectorAll("[imgviewlib-filename]");

        let ELs_getinfo = el.querySelectorAll("[imgviewlib-getinfo]");
        let ELs_load = el.querySelectorAll("[imgviewlib-load]");
        let ELs_delete = el.querySelectorAll("[imgviewlib-delete]");

        // console.log("elements to bind to", el, ELs_img, ELs_bg, ELs_filename, ELs_getinfo, ELs_load, ELs_delete);

        for (let el2I = 0; el2I < ELs_img.length; el2I++) {
            let el2 = ELs_img[el2I];
            el2.src = zitedata[datazite].thumbnails[filename];
        }

        for (let el2I = 0; el2I < ELs_bg.length; el2I++) {
            let el2 = ELs_bg[el2I];
            el2.style.backgroundImage = "url('" + zitedata[datazite].thumbnails[filename] + "')";
        }

        for (let el2I = 0; el2I < ELs_filename.length; el2I++) {
            let el2 = ELs_filename[el2I];
            el2.innerText = filename;
        }

        for (let el2I = 0; el2I < ELs_getinfo.length; el2I++) {
            let el2 = ELs_getinfo[el2I];
            el2.onclick = function() {
                checkDatafileInfo(datazite, _opt.datapath, filename);
            };
        }

        for (let el2I = 0; el2I < ELs_load.length; el2I++) {
            let el2 = ELs_load[el2I];
            el2.onclick = function() {
                requestDatafileLoad(datazite, _opt.datapath, filename, function() {
                    checkDatafileInfo(datazite, _opt.datapath, filename);
                });
            };
        }

        for (let el2I = 0; el2I < ELs_delete.length; el2I++) {
            let el2 = ELs_delete[el2I];
            el2.onclick = function() {
                requestDatafileDelete(datazite, filename);
            };
        }

        return {
            "img": ELs_img,
            "bg": ELs_bg,
            "filename": ELs_filename,
            "getinfo": ELs_getinfo,
            "load": ELs_load,
            "delete": ELs_delete
        };
    };

    elementBinder = function(datazite = opt.datazite, _opt = opt, filename, el) {
        let ELs = bindingUpdater(datazite, _opt, filename, el);

        if (!zitedata[datazite].hasOwnProperty("elements")) {
            zitedata[datazite].elements = {};
        }
        if (!zitedata[datazite].elements.hasOwnProperty(_opt.datapath + "/" + filename)) {
            zitedata[datazite].elements[_opt.datapath + "/" + filename] = [];
        }
        if (zitedata[datazite].elements[_opt.datapath + "/" + filename].indexOf(el) < 0) {
            zitedata[datazite].elements[_opt.datapath + "/" + filename].push({
                "el": el,
                "img": ELs.img,
                "bg": ELs.bg
            });
        }

        checkDatafileInfo(datazite, _opt.datapath, filename);

        el.classList.add("imgviewlib-loaded");
    };

    if (opt.datazite) {
        let datazite = opt.datazite;
        getThumbnails(datazite, opt.thumbnailspath, function() {
            for (let elI = 0; elI < ELs_file.length; elI++) {
                let el = ELs_file[elI];

                let filename = el.getAttribute("imgviewlib-file");

                elementBinder(datazite, opt, filename, el);
            }
        });
    }

    for (let elI = 0; elI < ELs_obj.length; elI++) {
        let el = ELs_obj[elI];

        let opt2 = JSON.parse(JSON.stringify(opt));
        let opt3 = JSON.parse(el.getAttribute("imgviewlib"));
        Object.assign(opt2, opt3);

        let filename = opt2.filename;
        let datazite = opt2.datazite;

        getThumbnails(datazite, opt2.thumbnailspath, function() {
            elementBinder(datazite, opt2, filename, el);
        });
    }
};
window.ImgViewLib = ImgViewLib;

/* ---- /1PmkreYrHALCPaPQx3h9Ht8oQzjCKpA3n/js/page.js ---- */


class Page extends ZeroFrame {
    setSiteInfo(site_info) {
        page.site_info = site_info;

        var out = document.getElementById("out");
        out.innerHTML =
            "Page address: " + site_info.address +
            "<br>- Peers: " + site_info.peers +
            "<br>- Size: " + site_info.settings.size +
            "<br>- Modified: " + (new Date(site_info.content.modified * 1000));
    }

    onOpenWebsocket() {
        this.cmd("siteInfo", [], function(site_info) {
            page.setSiteInfo(site_info);

            window.imgview = window.ImgViewLib(page);
        });
    }

    onRequest(cmd, message) {
        if (cmd == "setSiteInfo") {
            this.setSiteInfo(message.params);
        } else {
            this.log("Unknown incoming message:", cmd);
        }
    }
}
page = new Page();